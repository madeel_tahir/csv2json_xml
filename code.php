<?php
/*
Read the file 'source.csv' inside folder named 'input' and covert into Json and xml and generte output file.
*/
	
	$file = fopen("./input/source.csv", 'r');
	$outputFilename   = "target";
	// Get the headers of the file
	$headers = fgetcsv($file);
	/*
		Json File Generation
	*/
    $json['entry']=array();
    while ($row = fgetcsv($file,"1024",",")) {
        array_push($json['entry'],array_combine($headers, $row));
    }
	//fclose($file);
	file_put_contents("./output/json/".$outputFilename.".json", json_encode($json));	

	/*
		Json file generation end.
	*/

	/*
		Xml Generation
	*/

	$rows = array_map('str_getcsv', file("./input/source.csv"));
	$header = array_shift($rows);
	$data = array();
	foreach ($rows as $row)
	{
		$data[] = array_combine($header, $row);
	}

	$xml = new DomDocument();
	$root = $xml->createElement('entries');
	$xml->appendChild($root);

	// Add child nodes
	foreach($data as $key => $val) 
	{	
		$entry = $xml->createElement('entry');
		$root->appendChild($entry);
			
		foreach($val as $field_name => $field_value) 
		{	
			$field_name = preg_replace("/[^A-Za-z0-9]/", '', $field_name); // preg_replace has the allowed characters
			$name = $entry->appendChild($xml->createElement($field_name)); 
			$name->appendChild($xml->createCDATASection($field_value)); 
		}

	}

	// Set the formatOutput attribute of xml to true
	$xml->formatOutput = true; 
	// Save as file
	$xml->save("./output/xml/".$outputFilename.".xml"); // save as file

	fclose($file);
	echo "Completed! Check the output folder for the files.";
?>