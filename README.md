# Description

Docker image container, this will read a file inside the "input" folder and convert and output the JSON and XML in the "output" directory.

## Prerequisite

* Git installation. Download, install, and configure Git on your system.
* Docker installation. Make sure docker installed and running. 


## Usage

Execute the following command in the terminal to pull the code from repository.

```
git clone https://madeel_tahir@bitbucket.org/madeel_tahir/csv2json_xml.git
cd csv2json_xml
docker build -t "php-challenge" .
docker run --rm -v ${PWD}/:/output php-challenge
```

## REST API

Use the generated json file and upload.

[Frontend to consume the API service.](http://ec2-15-184-73-195.me-south-1.compute.amazonaws.com/challenge)


[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/339a00fd2861b3fad305)